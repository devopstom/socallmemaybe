package com.devopstom.socallmemaybe;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.auth.AccessToken;

/**
 * Hello world!
 *
 */
public class App 
{
    private static final Logger logger = LoggerFactory.getLogger(App.class);
    public static String twitterUsername = "";

    public static void main( String[] args )
    {
        //System.setProperty("twitter4j.loggerFactory", "twitter4j.internal.logging.NullLoggerFactory");

        try {
            if (args.length == 0){
               
                logger.error("You must set your username as the first argument");
                System.exit(1);
            }else {
                if (args.length == 1){
                  logger.error("You must also set the hashtags you wish to track as the following arguments");
                  logger.info("java -jar JDownloader.jar username PhoneNumber FollowTag1 Followtag..n");
                  System.exit(1);
                }
                twitterUsername = args[0];
                
                TwitterAuth ta = new TwitterAuth();
                
                AccessToken at = ta.loadAccessToken(twitterUsername);
                String notifyPhone = args[1];
                TwitterConsumer fs = new TwitterConsumer(at,notifyPhone);
                
                String[] lastArgs = Arrays.copyOfRange(args, 2, args.length);
                
                fs.filterKeywords(lastArgs);
                System.out.println("Waiting for Tweets.. Press q to quit.");
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                String lineInput = null;
                try {
                    lineInput = br.readLine();
                    if (lineInput.startsWith("q") || lineInput.startsWith("Q")){
                        fs.destroy();
                        System.exit(0);
                    }
                } catch (IOException ioe) {
                    System.out.println("IO error trying to read line input!");
                    ioe.printStackTrace();
                    System.exit(2);
                }

            }
            
        }catch(Exception ex){
            logger.error(ex.getLocalizedMessage(), ex);
            
        }
    }
}

