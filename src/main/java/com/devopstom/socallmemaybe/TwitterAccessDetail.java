/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.devopstom.socallmemaybe;

/**
 *
 * @author tom
 */
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author tom
 */
public class TwitterAccessDetail {
    private long uid;
    private String screenName;
    private String key;
    private String secret;
    private static final Logger logger = LoggerFactory.getLogger(TwitterAccessDetail.class);


    
    public String getKey(){
        try{
            return this.key;
        }catch (Exception ex){
            logger.error(ex.getLocalizedMessage(), ex);
            return "";
        }
    }
    public String getScreenName(){
        try{
            return this.screenName;
        }catch (Exception ex){
            logger.error(ex.getLocalizedMessage(), ex);
            return "";
        }
    }
    public String getSecret(){
        try{
            return this.secret;
        }catch (Exception ex){
            logger.error(ex.getLocalizedMessage(), ex);
            return "";
        }
    }
    
    public void setScreenName(String sn){
        try{
            this.screenName = sn;
        }catch (Exception ex){
            logger.error(ex.getLocalizedMessage(), ex);
        }
    }
    
    public void setKey(String _key){
        try{
            this.key = _key;
        }catch (Exception ex){
            logger.error(ex.getLocalizedMessage(), ex);
        }
    }
    
    public void setSecret(String _secret){
        try {
            this.secret = _secret;
        }catch (Exception ex){
            logger.error(ex.getLocalizedMessage(), ex);
        }
    }
    
    public void setUid(long _uid){
        try{
            this.uid = _uid;
        }catch (Exception ex){
            logger.error(ex.getLocalizedMessage(), ex);
        }
    }
    public long getUid(){
        try{
            return this.uid;
        }catch (Exception ex){
            logger.error(ex.getLocalizedMessage(), ex);
            return 0;
        }
    }
    
    public TwitterAccessDetail(long _uid, String _key, String _secret, String _sn){
        

        this.uid = _uid;
        this.key = _key;
        this.secret = _secret;
        this.screenName = _sn;

    }
    
    @Override
    public String toString(){
        try{
        StringBuilder sb = new StringBuilder();
        sb.append("TwitterAccessDetail:").append(this.uid).append("(").append(this.screenName).append(")").append("=>{");
        sb.append("key:").append(this.key);
        sb.append(",");
        sb.append("secret:").append(this.secret);
        sb.append("}");
             
        return sb.toString();
        }catch (Exception ex){
            logger.error(ex.getLocalizedMessage(), ex);
            
            return "";
        }
    }
    
    public Map<String,String> toMap(){
        Map<String,String> s = new HashMap<String,String>();
        s.put("uid", String.valueOf(this.uid));
        s.put("key",this.key);
        s.put("secret",this.secret);
        s.put("screenName", this.screenName);
        return s;
    }
    
//    public static TwitterAccessDetail fromMap(Map<String,String> _s){
//        return TwitterAccessDetail(_s.get("uid"))
//    }
    
        
}

