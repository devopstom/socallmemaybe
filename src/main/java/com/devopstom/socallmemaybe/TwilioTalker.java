/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.devopstom.socallmemaybe;

/**
 *
 * @author tom
 */


import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.TwilioRestResponse;
import com.twilio.sdk.resource.factory.CallFactory;
import com.twilio.sdk.resource.factory.SmsFactory;
import com.twilio.sdk.resource.instance.Account;
import com.twilio.sdk.resource.instance.AvailablePhoneNumber;
import com.twilio.sdk.resource.instance.Call;
import com.twilio.sdk.resource.instance.Conference;
import com.twilio.sdk.resource.instance.Participant;
import com.twilio.sdk.resource.list.AccountList;
import com.twilio.sdk.resource.list.AvailablePhoneNumberList;
import com.twilio.sdk.resource.list.ParticipantList;
import com.twilio.sdk.verbs.TwiMLException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class TwilioTalker {
    private static final String ACCOUNT_SID = "ACc52f2937e0a03a37c687ceaefa177188";
    private static final String AUTH_TOKEN = "c65c82167d9afa25596801ce41e0eb28";
    private static final String FROM_NUMBER = "+442033229823";
    public final TwilioRestClient client; 
        private static final Logger logger = LoggerFactory.getLogger(TwilioTalker.class);

    public TwilioTalker(){
        client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);
    }
    
    
    public boolean call(String callRecipient, String message){
        final Account mainAccount = client.getAccount();
        final CallFactory callFactory = mainAccount.getCallFactory();
        final Map<String, String> callParams = new HashMap<String, String>();
        callParams.put("To", callRecipient); // Replace with a valid phone number
        callParams.put("From", FROM_NUMBER); // Replace with a valid phone number in your account
        TwilioResponseBuilder trb = new TwilioResponseBuilder();
        try {
            String uploadedUrl = trb.uploadResponse(message).toString();
            callParams.put("Url", uploadedUrl);
            callParams.put("Method","GET");
            callParams.put("IfMachine","Continue");
            logger.info("UPLOADED_URL : " + uploadedUrl);
            final Call call = callFactory.create(callParams);
            return true;
            
        } catch (MalformedURLException ex) {
            logger.error(ex.getLocalizedMessage(),ex);
            return false;
        } catch (TwilioRestException ex) {
            logger.error(ex.getLocalizedMessage(),ex);
            return false;
        }
        
        
    }
    
    
}
