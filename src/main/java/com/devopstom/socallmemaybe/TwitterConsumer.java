/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.devopstom.socallmemaybe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.*;
import twitter4j.auth.AccessToken;

/**
 *
 * @author tom
 */
public class TwitterConsumer {
    private static final String consumerKey = "hBAWCkpfcCFAgxIRQWDBA";
    private static final String consumerSecret = "3U9js9XQjauGWLSuew9OStGfAqlWhnVtHSrvCSgXhU";
    private static final Logger logger = LoggerFactory.getLogger(TwitterConsumer.class);
    private static TwitterStream twitterStream;
    private TwilioTalker tt;
    private String notifyPhone;
    public TwitterConsumer(AccessToken at, String notifyPhoneNumber)
    {        
        notifyPhone = notifyPhoneNumber;
        twitterStream = TwitterStreamFactory.getSingleton();
        twitterStream.setOAuthConsumer(consumerKey, consumerSecret);
        twitterStream.setOAuthAccessToken(at);
        tt = new TwilioTalker();

    }
    
    public void destroy(){
        try{
            twitterStream.cleanUp();
            twitterStream.shutdown();
        }catch (Exception ex){
            logger.error(ex.getLocalizedMessage(), ex);
            ex.printStackTrace();
        }
    }
    
    public void filterKeywords(String[] keywords) { 
        try {
            //global_keywords = keywords;
        StatusListener listener = new StatusListener() {
            @Override
            public void onStatus(Status status) {
                //boolean call = tt.call("447885886595",status.getText());
                boolean call = tt.call(notifyPhone,status.getText());
                logger.info("@" + status.getUser().getScreenName() + " - " 
                        + status.getText());
                    
            }

            @Override
            public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) 
            {
                logger.info("Got a status deletion notice id:" + 
                        statusDeletionNotice.getStatusId());
            }

            @Override
            public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
                logger.info("Got track limitation notice:" + 
                        numberOfLimitedStatuses);
            }

            @Override
            public void onScrubGeo(long userId, long upToStatusId) {
                logger.info("Got scrub_geo event userId:" + 
                        userId + " upToStatusId:" + upToStatusId);
            }

            @Override
            public void onStallWarning(StallWarning warning) {
                logger.info("Got stall warning:" + warning);
            }

            @Override
            public void onException(Exception ex) {
                logger.error(ex.getLocalizedMessage(), ex);
                ex.printStackTrace();
            }
        };
        twitterStream.addListener(listener);
        FilterQuery filterQuery = new FilterQuery();
        filterQuery.track(keywords);
        twitterStream.filter(filterQuery);
        } catch (Exception ex){

            logger.error(ex.getLocalizedMessage(), ex);
            ex.printStackTrace();
        }
    }

}