/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.devopstom.socallmemaybe;

import com.amazonaws.HttpMethod;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.Date;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author tom
 */
public class TwilioResponseBuilder {
    private static final String AWS_ACCESS_KEY = "AKIAJCZXPAZ7E6HOC5ZQ";
    private static final String AWS_ACCESS_SECRET = "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";
    private static final String AWS_BUCKET = "socallmemaybe2013";
    private static final Logger logger = LoggerFactory.getLogger(TwilioResponseBuilder.class);

    public URL uploadResponse(String message) throws MalformedURLException{
        try {
            AmazonS3Client s3Client = new AmazonS3Client( new BasicAWSCredentials( AWS_ACCESS_KEY, AWS_ACCESS_SECRET ));
            s3Client.createBucket( AWS_BUCKET );

            StringBuilder tmpl = new StringBuilder();
            tmpl.append("<Response>\n");
            tmpl.append("<Say>\n");
            tmpl.append(message);
            tmpl.append("\n");
            tmpl.append("</Say>\n");
            tmpl.append("</Response>\n");
            InputStream is = new ByteArrayInputStream(tmpl.toString().getBytes());
            ObjectMetadata md = new ObjectMetadata();
            md.setContentLength(tmpl.toString().length());
            String uuidName = UUID.randomUUID().toString();
            PutObjectRequest por = new PutObjectRequest( AWS_BUCKET, uuidName , is,md);
            por.setCannedAcl(CannedAccessControlList.PublicRead);
            PutObjectResult res = s3Client.putObject( por );
            ResponseHeaderOverrides override = new ResponseHeaderOverrides();
            override.setContentType( "text/xml" );

            GeneratePresignedUrlRequest urlRequest = new GeneratePresignedUrlRequest( AWS_BUCKET, uuidName);
            urlRequest.setMethod(HttpMethod.GET);
            urlRequest.setExpiration( new Date( System.currentTimeMillis() + 3600000 ) );  // Added an hour's worth of milliseconds to the current time.
            urlRequest.setResponseHeaders( override );

            URL url = s3Client.generatePresignedUrl( urlRequest );
            return url;
        }catch (Exception ex){
            logger.error(ex.getLocalizedMessage(), ex);
            return new URL("http://localhost");
            
        }

    }
    
}
